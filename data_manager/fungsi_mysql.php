<?php 
//koneksi ke database
$conn = mysqli_connect("localhost","root","","unpas");


//ambil data mahasiswa / query data mahasiswa


function query ($query){ 
	global $conn;

	$result = mysqli_query($conn, $query);

	$rows=[];

	while ($row = mysqli_fetch_array($result) ) {

		$rows[]= $row;
		
	}

	return $rows; 			

}


function tambah($data){
global $conn;
	//ambil data
	//simpan dalam variabel karena kutipnya numpuk di query
	// fungsi sekuriti sederhana supaya tidak dihack htmlspecialchars(string)
	$nrp = htmlspecialchars($data["nrp"]);
	$nama = htmlspecialchars($data["nama"]);
	$email= htmlspecialchars($data["email"]);
	$jurusan = htmlspecialchars($data["jurusan"]);

	//upload gambar
	$gambar = upload();
	if ( !$gambar){

		return false;
	}

	//query insert data
	$query = "INSERT INTO mahasiswa
				VALUES
				('','$nama','$nrp','$email','$jurusan','$gambar')
			";

	mysqli_query($conn, $query);

	return mysqli_affected_rows($conn);
}



function upload (){
	$namafile = $_FILES['gambar']['name'];
	$ukuranfile = $_FILES['gambar']['size'];
	$error = $_FILES['gambar']['error'];		
	$temp = $_FILES['gambar']['tmp_name'];

	//cek apakah tidak ad gambar yang diupload
	if($error === 4){

		echo "<script>
					alert('pilih gambar terlebih dahulu');
				</script>";

		return false;
	}


	//cek apakah ini gambar
	$formatgambar = ['jpg','jpeg','png'];
	$ekstensigambar = explode('.',$namafile);
	//strtolower = membuat string jadi huruf kecil, end($variabel) = supaya mengambil titik yang paling akhir
	$ekstensigambar = strtolower(end($ekstensigambar));

	if(!in_array( $ekstensigambar , $formatgambar )){

		echo "<script>
					alert('yang Anda upload bukan gambar');
				</script>";

		return false;
	}



	//cek jika ukurannya terlalu besar
	//size dalam byte
	if ($ukuranfile > 100000){

		echo "<script>
					alert('ukuran gambar terlalu besar');
				</script>";

		return false;	
	}


	//lolos pengeceka, gambar siap diupload
	
	//generate nama baru
	$namafilebaru = uniqid();
	$namafilebaru .= '.';
	$namafilebaru .= $ekstensigambar;
	//filenya itu sudah masuk di unpas/
	move_uploaded_file($temp , 'img/'.$namafilebaru);


	return $namafilebaru;

}



function hapus ($id){
	global $conn;

	mysqli_query($conn,"DELETE FROM mahasiswa WHERE id = $id");



	return mysqli_affected_rows($conn); 

}

function ubah ($data){
global $conn;
	//ambil data
	//simpan dalam variabel karena kutipnya numpuk di query
	// fungsi sekuriti sederhana supaya tidak dihack htmlspecialchars(string)
	$id = $data["id"];
 	$nrp = htmlspecialchars($data["nrp"]);
	$nama = htmlspecialchars($data["nama"]);
	$email= htmlspecialchars($data["email"]);
	$jurusan = htmlspecialchars($data["jurusan"]);
	$gambarlama = htmlspecialchars($data["gambarlama"]);

	//cek tombol upload
	if( $_FILES['gambar']['error'] === 4){
		$gambar = $gambarlama;	
	}else{

		$gambar = upload();
	}


	//query insert data
	$query = "UPDATE mahasiswa SET
				nrp = '$nrp',
				nama = '$nama',
				email = '$email',
				jurusan = '$jurusan',
				gambar = '$gambar'

				WHERE id = $id
			";

	mysqli_query($conn, $query);

	return mysqli_affected_rows($conn);
}


function cari($keyword){

	$query = "SELECT * FROM mahasiswa WHERE

				nama LIKE '%$keyword%' OR
				nrp LIKE '%$keyword%'	 OR
				jurusan LIKE '$keyword%' OR
				nama LIKE '$keyword%'
			";
//like(sama sepert), keyword%(cari dari depan)

return query($query);
}


function registrasi ($data){
	global $conn;

	$username = strtolower(stripcslashes($data["username"]));

	// mysqli_real_escape_string() = biar kutip juga bisa masuk
	$password = mysqli_real_escape_string($conn, $data["password"]);
	$password2 = mysqli_real_escape_string($conn, $data["password2"]);

	//cek kesamaan username
	$result = mysqli_query($conn, "SELECT username FROM user WHERE username = '$username' ");

	if( mysqli_fetch_array($result) ){

		echo "<script>

			alert('username sudah terdaftar');

			</script>";

		return false;
	}

	//cek konfirmasi password

	if ($password !== $password2){

		echo "<script>

			alert('Konfirmasi password Tidak Sesuai');

			</script>";

			return false;
	}

	//enkripsi password
	// $password = md5($password); (tidak disarankan, karena bisa dilihat di web)

	$password = password_hash($password, PASSWORD_DEFAULT);

	mysqli_query($conn, "INSERT INTO user VALUES('','$username','$password')");

	return mysqli_affected_rows($conn);
	//tambahkan user baru
}



 ?>