//ambil elemen
var keyword = document.getElementById('keyword');
var tombolcari = document.getElementById('tombolcari');
var container = document.getElementById('container');


//tambahkan event ketika keyword ditulis
keyword.addEventListener('keyup',function(){

	//buat objek ajax
	var ajax = new XMLHttpRequest();


	//cek siap ajax
	ajax.onreadystatechange = function(){

		if(ajax.readyState == 4 && ajax.status == 200){
			//megnubah isi containernya ke isi dari sumber ajaxnya
			container.innerHTML=ajax.responseText;


		}
	}

	//eksekusi ajax
	ajax.open('GET','ajax/ajax.php?keyword=' + keyword.value, true);	
	ajax.send();

});