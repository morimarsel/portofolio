//$ = jQuery
$(document).ready(function(){

	$('#tombolcari').hide();
	//event ketika keyword 	ditulis
	$('#keyword').on('keyup',function(){

		//munculkan icon loading
		$('.loader').show();


		// //ajax menggunakan load
		// $('#container').load('ajax/ajax.php?keyword=' + $('#keyword').val());

		//val() = isi dari ketikkan kita

		//$.get()
		$.get('ajax/ajax.php?keyword=' + $('#keyword').val(), function(data){

			//data adalalah parameter get nya

			$('#container').html(data);
			//menghilangkan loader
			$('.loader').hide();
		});

	});

});

