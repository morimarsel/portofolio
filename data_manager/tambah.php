<?php 

session_start();

if ( !isset($_SESSION["login"]) ) {

	header("Location: login.php");
	exit;

}

include 'fungsi_mysql.php';



if (	isset($_POST["submit"])) {
	
	

	if ( tambah($_POST) > 0) {


		//sedikit sentuhan javascript

		echo "

			<script>
					alert('Data Berhasil Ditambahkan!');
					document.location.href = 'live_search(jquery).php';
			</script>


		";


		// echo "Data Berhasil ditambahkan";

		// sleep(3);

		// header("Location: php_dan_mysql(index).php");

	}else{

		echo "

			<script>
					alert('Data Gagal Ditambahkan!');
					document.location.href = 'live_search(jquery).php';
			</script>


		";
	}

}

 ?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form by Colorlib</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="css/style2.css">
</head>
<body>

    <div class="main">

        <h1>Mahasiswa</h1>
        <div class="container">
            <div class="sign-up-content">
                <form action="" method="post" enctype="multipart/form-data">
                    <h2 class="form-title">Tambah Data</h2>

                    <div class="form-textbox">
                        <label for="nrp">NRP: </label>
						<input type="text" name="nrp" id="nrp" required>
                    </div>

                    <div class="form-textbox">
                    	<label for="nama">Nama </label>
						<input type="text" name="nama" id="nama">
                    </div>

                    <div class="form-textbox">
                        <label for="email">E-mail: </label>
						<input type="text" name="email" id="email">
                    </div>

                    <div class="form-textbox">
                        <label for="jurusan">Jurusan: </label>
						<input type="text" name="jurusan" id="jurusan">
                    </div>

                    <div class="form-textbox">
                        <label for="gambar">Gambar: </label>
						<input type="file" name="gambar" id="gambar">
                    </div>

                    <div class="form-textbox">
                        <input type="submit" name="submit" id="submit" class="submit" value="TAMBAHKAN" />
                    </div>
                </form>

                <p class="loginhere">
                    <a href="/mahasiswa/live_search(jquery).php" class="loginhere-link"> Kembali</a>
                </p>
            </div>
        </div>

    </div>

    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main3.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>



<!-- <!DOCTYPE html>
<html>
<head>
	<title>Tambah Data Mahasiswa</title>
</head>
<body>

	<h1>Tambah Data Mahasiswa</h1>

	enctype untuk membuat jalur gambar berbeda dengan string, dan masuk ke $_FILES
	<form action="" method="post" enctype="multipart/form-data">
		<ul style="list-style-type: none;">
			<li>
				<label for="nrp">NRP: </label>
				<input type="text" name="nrp" id="nrp" required>
			</li>
			<li>
				<label for="nama">Nama: </label>
				<input type="text" name="nama" id="nama">
			</li>
			<li>
				<label for="email">E-mail: </label>
				<input type="text" name="email" id="email">
			</li>
			<li>
				<label for="jurusan">Jurusan: </label>
				<input type="text" name="jurusan" id="jurusan">
			</li>
			<li>
				<label for="gambar">Gambar: </label>
				<input type="file" name="gambar" id="gambar">
			</li>
			<li>
				<button type="submit" name="submit">Tambah Data</button>
			</li>
		</ul>


	</form>

</body>
</html> -->