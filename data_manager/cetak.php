<?php

require_once __DIR__ . '/vendor/autoload.php';

include 'fungsi_mysql.php';

$result = query("SELECT * FROM mahasiswa");

$html = '<!DOCTYPE html>
		<html>
		<head>
			<title>Halaman Admin</title>
		</head>
		<body>

				<h1>Daftar Mahasiswa</h1>

				<table border="3" cellpadding="15" cellspacing="0">
				<tr>
					<th>No.</th>
					<th>NRP</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Jurusan</th>
					<th>Gambar</th>
				</tr>';

$i = 1;
foreach ($result as $row) {
				$html .=
				'<tr><td>'
				. $i++ .
				'</td><td>'
				. $row["nrp"] .
				'</td><td>'
				. $row["nama"] .
				'</td><td>'
				. $row["email"] .
				'</td><td>'
				. $row["jurusan"] 
				.'</td><td><img src="img/'
				. $row["gambar"].
				' " width="50px" ></td></tr>';
		}

$html .='		</table>
		</body>
		</html>';


$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML($html);
$mpdf->Output('Daftar-mahasiswa.pdf', \Mpdf\Output\Destination::DOWNLOAD);

?>

