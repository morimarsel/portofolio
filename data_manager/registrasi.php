<?php 
include 'fungsi_mysql.php';

if (isset($_POST["register"])){

	if(registrasi($_POST) > 0){

		echo "<script>

				alert('user baru berhasil ditambahkan!, Silahkan login');
				document.location.href = 'login.php';

			</script>";

	} else{

		echo mysqli_error($conn);
	}
}

 ?>



<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Halaman Registrasi</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/register.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body">
                    <h2 class="title">Daftar</h2>
                    <form method="POST">
                        
                        <div class="input-group">
                            <label class="label" for="username">Nama</label>
                            <input class="input--style-4" type="text" name="username" id="username">
                        </div>
                           
                            
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label" for="password">Password</label>
                                    <input class="input--style-4" type="password" name="password" id="password">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label" for="password2" >Ketik ulang password</label>
                                    <input class="input--style-4" type="password" name="password2" id="password2">
                                </div>
                            </div>
                        </div>
                        
	                    <div class="flex-sb-m w-full p-t-3 p-b-24">

							<div>
								<a href="login.php" class="txt1">	
									Sudah punya akun
								</a>
							</div>

						</div>
                 
                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--blue" type="submit" name="register">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->


<!-- <!DOCTYPE html>
<html>
<head>
	<title>Halaman Registrasi</title>
	<style>
		label {

			display: block;
		}
	</style>
</head>
<body>
	<h1>Halaman Register</h1>

<form action="" method="post">
	<ul style="list-style-type: none;">
		<li>
			<label for="username">Username:  </label>
			<input type="text" name="username" id="username"> </label>
		</li>
		<li>
			<label for="password">Password:  </label>
			<input type="password" name="password" id="password">
		</li>
		<li>
			<label for="password2">Ketik Ulang Password:  </label>
			<input type="password" name="password2" id="password2">
		</li>
		<li>
			<button type="submit" name="register">Daftar!!!</button>
		</li>
	</ul>

</form>

<a href="login.php"><b><p>Sudah Punya Akun?</p></b></a>

</body>
</html>
 -->