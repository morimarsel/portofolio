<?php 
session_start();

if ( !isset($_SESSION["login"]) ) {

	header("Location: login.php");
	exit;

}


include 'fungsi_mysql.php';


$jumlahdataperhalaman = 2;
$jumlahdata = count(query("SELECT * FROM mahasiswa"));
//pembulatan round(normal), floor(kebawah), ceil(keatas)
$jumlahhalaman = ceil( $jumlahdata/$jumlahdataperhalaman );
$halamanaktif = ( isset($_GET["halaman"]) ) ? $_GET["halaman"] : 1;
$awaldata = ($jumlahdataperhalaman *  $halamanaktif ) - $jumlahdataperhalaman;



$result = query("SELECT * FROM mahasiswa");

//tombol ditekan
if (isset( $_POST["cari"]) ) {

	$result = cari($_POST["keyword"]);
}


?>



<!DOCTYPE html>
<html lang="en">
<head>
	<title>Mahasiswa</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		.loader {
			width: 100px;
			position: absolute;
			top: 135px;
			left: 440px;
			z-index: -1;
			display: none;
		}


		/*@media = untuk membuat pdf saat di print berubah*/
		@media print {	

			.logout{
				display: none;
			}

		}


		a{
		    text-decoration:none;
		}

		</style>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons2/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util2.css">
	<link rel="stylesheet" type="text/css" href="css/main2.css">
<!--===============================================================================================-->

<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/style.css">

</head>
<body>
	
	<div class="limiter">
		
			
						

		<section class="ftco-section">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-6 text-center mb-5">
						<h2 class="heading-section">DAFTAR MAHASISWA</h2>
					</div>
				</div>

				<form action="" method="post">
					<input type="text" name="keyword" size="60" autofocus placeholder="Cari Data" autocomplete="off" id="keyword">

					<button type="submit" name="cari" id="tombolcari">Cari!!</button>
					<img src="img/loader.gif" class="loader">

				</form>

				<br>

				<div class="dropdown " style="size: 100px;">
					<button class="btn btn-secondary dropdown-toggle btn-lg btn-info" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
					    MENU
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">

					    <li>
					    	<a class="dropdown-item" href="tambah.php">
					    		<i class="fa fa-folder" style="font-size:20px;color:blue;"></i>
					    		TAMBAH DATA
					    	</a>
					    </li>
					    <li>
					    	<a class="dropdown-item" href="cetak.php" target="_blank">
					    		<i class="fa fa-print" style="font-size:20px;color:black"></i>CETAK
					    	</a>
					    </li>
					    <li>
					    	<a class="dropdown-item" href="logout.php">
					    		<i class="fa fa-sign-out" style="font-size:20px;color:red"></i> LOGOUT
					    	</a>
					    	
					    </li>
					</ul>
				</div>

				<div class="row">
					<div class="col-md-12">
						
						<div class="table">
							<div id="container">
								<table class="table">
								    <thead class="thead-primary">
									     <tr>
									      	<th>No.</th>
									      	<th>Gambar</th>
											<th>NRP</th>
											<th>Nama</th>
											<th>Email</th>
											<th>Jurusan</th>
											<th>Aksi</th>
									    </tr>
								    </thead>

								    <tbody>
								    	<?php $i=1; ?>	
										<?php foreach ($result as $row) { ?>

										    <tr>
										        <td>

										        	<?php echo $i; ?>
										        </td>
										        <td>
													<img class="img-fluid img-thumbnail" src="img/<?php print $row["gambar"];?>">
												</td>
												

												<td>
													<?= $row["nrp"];?>
														
												</td>
												<td>
													<?php echo $row["nama"]; ?>
														
												</td>
												<td>
													<?php echo $row["email"]; ?>
														
													</td>
												<td>
													<?php print $row["jurusan"]; ?>
													
												</td>

												<td>
													<button type="button" class="btn btn-warning">
														<a style="color: black;" href="ubah.php?id=<?php echo $row["id"];?>">Ubah</a>
													</button>
													<button type="button" class="btn btn-danger">	<a style="color: black;" href="hapus.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('yakin?');">

															Hapus

														</a>
													</button>
												</td>
												
										    </tr>
									    <?php $i++; ?>

										<?php }?>  
								    </tbody>
							    </table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

					
		
	</div>

	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})

			$(this).on('ps-x-reach-start', function(){
				$(this).parent().find('.table100-firstcol').removeClass('shadow-table100-firstcol');
			});

			$(this).on('ps-scroll-x', function(){
				$(this).parent().find('.table100-firstcol').addClass('shadow-table100-firstcol');
			});

		});

		
		
		
	</script>
	<!--===============================================================================================-->
	<script src="js/main2.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/main2.js"></script>
	<script src="js/script_jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.bundle.js"></script>

</body>
</html>
