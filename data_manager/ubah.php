<?php  

session_start();

if ( !isset($_SESSION["login"]) ) {

	header("Location: login.php");
	exit;

}


include 'fungsi_mysql.php';

//ambil data di url

$id = $_GET["id"];

//query data mahasiswa berdasarkan query
$mhs = query("SELECT * FROM mahasiswa WHERE id = $id")[0];



$conn = mysqli_connect("localhost","root","","unpas");


if (isset($_POST["submit"])) {
		 

	if ( ubah($_POST) > 0) {


		//sedikit sentuhan javascript

		echo "

			<script>
					alert('Data Berhasil Diubah!');
					document.location.href = 'live_search(jquery).php';
			</script>


		";


		// echo "Data Berhasil ditambahkan";

		// sleep(3);

		// header("Location: php_dan_mysql(index).php");


	}else{

		echo "

			<script>
					alert('Data Gagal Diubah!');
					document.location.href = 'live_search(jquery).php';
			</script>


		";
	}

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Ubah Data</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons3/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main4.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="img/<?php echo $mhs["gambar"]; ?>">
				</div>




				<form action="" method="post" enctype="multipart/form-data">


					<input type="hidden" name="id" value="<?php echo $mhs["id"] ?>">

					<input type="hidden" name="gambarlama" value="<?php echo $mhs["gambar"] ?>">


					<span class="login100-form-title">
						Ubah Data
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input placeholder="NRP" class="input100" type="text" name="nrp" id="nrp" required value="<?php echo $mhs["nrp"] ?>">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-id-card" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" placeholder="Email" placeholder="Nama" type="text" name="nama" id="nama" value="<?php echo $mhs["nama"] ?>">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-id-badge" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" placeholder="E-mail" type="text" name="email" id="email" value="<?php echo $mhs["email"] ?>">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" placeholder="Jurusan" type="text" name="jurusan" id="jurusan" value="<?php echo $mhs["jurusan"] ?>">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-angle-double-right" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input type="file" name="gambar" id="gambar" class="btn btn-secondary">
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" name="submit">
							Ubah
						</button>
					</div>

					<div class="text-center p-t-12">
						<a class="txt2" href="/mahasiswa/live_search(jquery).php">
							Kembali
						</a>
					</div>

				</form>




			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="js/main4.js"></script>

</body>
</html>













<!-- <!DOCTYPE html>
<html>
<head>
	<title>Ubah Data Mahasiswa</title>
</head>
<body>

	<h1>Ubah Data Mahasiswa</h1>


	<form action="" method="post" enctype="multipart/form-data">

		<input type="hidden" name="id" value="<?php echo $mhs["id"] ?>">

		<input type="hidden" name="gambarlama" value="<?php echo $mhs["gambar"] ?>">
		
		<ul style="list-style-type: none;">
			<li>

				<label for="nrp">NRP: </label>
				<input type="text" name="nrp" id="nrp" required value="<?php echo $mhs["nrp"] ?>">	
			</li>
			<li>
				<label for="nama">Nama: </label>
				<input type="text" name="nama" id="nama" value="<?php echo $mhs["nama"] ?>">
			</li>
			<li>
				<label for="email">E-mail: </label>
				<input type="text" name="email" id="email" value="<?php echo $mhs["email"] ?>">
			</li>
			<li>
				<label for="jurusan">Jurusan: </label>
				<input type="text" name="jurusan" id="jurusan" value="<?php echo $mhs["jurusan"] ?>">
			</li>
			<li>
				<label for="gambar">Gambar: </label><br>

				<img src="img/<?php echo $mhs["gambar"]; ?>" width="100px"><br>

				<input type="file" name="gambar" id="gambar">
			</li>
			<li>
				<button type="submit" name="submit">Ubah Data</button>
			</li>
		</ul>


	</form>


</body>
</html>	 -->