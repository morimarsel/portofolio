<?php 
require '../fungsi_mysql.php';
$keyword = $_GET["keyword"];

		
$result = cari($keyword);
?>

<table class="table">
    <thead class="thead-primary">
	     <tr>
	      	<th>No.</th>
	      	<th>Gambar</th>
			<th>NRP</th>
			<th>Nama</th>
			<th>Email</th>
			<th>Jurusan</th>
			<th>Aksi</th>
	    </tr>
    </thead>

    <tbody>
    	<?php $i=1; ?>	
		<?php foreach ($result as $row) { ?>

		    <tr>
		        <td>

		        	<?php echo $i; ?>
		        </td>
		        <td>
					<img class="img-fluid img-thumbnail" src="img/<?php print $row["gambar"];?>">
				</td>
				

				<td>
					<?= $row["nrp"];?>
						
				</td>
				<td>
					<?php echo $row["nama"]; ?>
						
				</td>
				<td>
					<?php echo $row["email"]; ?>
						
					</td>
				<td>
					<?php print $row["jurusan"]; ?>
					
				</td>

				<td>
					<button type="button" class="btn btn-warning">
						<a style="color: black;" href="ubah.php?id=<?php echo $row["id"];?>">Ubah</a>
					</button>
					<button type="button" class="btn btn-danger">	<a style="color: black;" href="hapus.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('yakin?');">

							Hapus

						</a>
					</button>
				</td>
				
		    </tr>
	    <?php $i++; ?>

		<?php }?>  
    </tbody>
</table>