<?php 
session_start();
require 'fungsi_mysql.php';

//cek cookie
if (isset($_COOKIE['id']) AND isset($_COOKIE['username']) ) {

	$id = $_COOKIE['id'];
	$username = $_COOKIE['username'];
	

	//ambil username berdasarkan id
	$result = mysqli_query($conn, "SELECT username FROM user WHERE id = $id");
	$row = mysqli_fetch_array($result);

	//cek cookie dan username
	if ($username === hash('sha256', $row['username']) ) {

		$_SESSION['login']= true;

	}

}

if ( isset($_SESSION["login"]) ) {

	header("Location: live_search(jquery).php");
	exit;
	
}




	if (isset($_POST["login"])){

		$username = $_POST["username"];
		$password = $_POST["password"];

		$result = mysqli_query($conn,"SELECT * FROM user WHERE username = '$username' ");

		//cek username
		if(mysqli_num_rows($result) === 1 ){

			//cek password
			$row = mysqli_fetch_array($result);

			if(password_verify( $password , $row["password"] )){

				//set session
				$_SESSION["login"] = true;

				//cek remember me
				if (isset ($_POST['remember']) ) {
					//buat cookie

					setcookie('id',$row['id'],time()+120);
					setcookie('username',hash('sha256', $row['username']), time()+120 );
				}

				header("Location: live_search(jquery).php");
			}
		}


		$error = true;

	}

 ?>

 <!DOCTYPE html>
<html lang="en">
<head>
	<title>Halaman login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
				<form class="login100-form validate-form flex-sb flex-w" method="post">
					<span class="login100-form-title p-b-51">
						Login
					</span>

					

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
						<input class="input100" type="text" name="username" id="username" placeholder="Username">
						<span class="focus-input100"></span>
					</div>
					
					
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="password" id="password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>

				

					<div class="flex-sb-m w-full p-t-3 p-b-24">
						<div class="contact100-form-checkbox">

							<input class="input-checkbox100" name="remember" id="remember" type="checkbox">
							<label class="label-checkbox100" for="remember">
								Remember me
							</label>

						</div>

						<div>
							<a href="registrasi.php" class="txt1">	
								Buat Akun
							</a>
						</div>
					</div>
				

					<?php if(isset($error)):?>
					 	<p style="color: red; font-style: italic;">username/password salah</p>
					<?php endif ?>

					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn" type="submit" name="login">
							Login
						</button>
					</div>
				
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>

<!-- <!DOCTYPE html>
 <html>
 <head>
 	<title>Halaman login</title>
 </head>
 <body>

 <h1>Halaman Login</h1>

 <?php if(isset($error)):?>
 	<p style="color: red; font-style: italic;">username/password salah</p>
 <?php endif ?>

 <form action="" method="post">
 	
 	<ul style="list-style-type: none;">
 		<li>
 			<label for="username">
 				Username: 
 			</label>
 			<input type="text" name="username" id="username">
 		</li>
 		<li>
 			<label for="password">
 				Password: 
 			</label>
 			<input type="password" name="password" id="password">
 		</li>
 		<li>
 			<input type="checkbox" name="remember" id="remember">
 			<label for="remember">
 				Remember Me 
 			</label>
 			
 		</li>
 		<li>
 			<button type="submit" name="login">Login!!!</button>
 		</li>

 	</ul>

</form>

<a href="registrasi.php"><p>Belum Punya Akun?</p></a>

 </body>
 </html> -->