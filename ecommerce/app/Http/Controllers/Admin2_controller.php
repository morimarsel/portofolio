<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Models\product;
use App\Models\category;
use App\Models\user;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Redirect;	

class Admin2_controller extends Controller
{
    public function index ()
	{
		isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

		$title ='AdminLTE 3 | Dashboard 2';

		$user = user::all();		

		return view('admin_view.admin2', ['title' => $title, 'user' => $user, 'username' => $username ] );
    }

    public function insert (){

		
		return view('admin_view.insert_admin2');

	}

	// request itu syaratnya kalau pakai post
	public function insertAction (Request $request)
	{	

		// nama namnya harus sama dengan name di form post
		// $request itu kayak variabe globalnya
		$validated = $request->validate([

			'nama' => 'required',
			'email' => 'required',
			'password' => 'required',
			

		]);

		// kalau isian salah keputus disini


		$user = new user;
		$user->name = $request->input('nama');
		$user->email = $request->input('email');
		$user->password = $request->input('password');
		$user->save();
		
		

		return \Redirect::to('/admin/admin2/')->with('alert', 'Sukses');
	}

	public function edit (Request $request){

		$id = $request->id;	
		$user = user::find($id);
		
		
		return view('admin_view.edit_admin2',compact('user'));
	}

	// request itu syaratnya kalau pakai post
	public function editAction (Request $request)
	{	
		$id = $request->id;	
		$user = user::find($id);

		$validated = $request->validate([

			'nama' => 'required',
			'email' => 'required',
			'password' => 'required',
			

		]);

		// kalau isian salah keputus disini

		$user->name = $request->input('nama');
		$user->email = $request->input('email');
		$user->password = $request->input('password');
		$user->save();
		
		

		return \Redirect::to('/admin/admin2/')->with('alert_update', 'Sukses');
	}	


	public function delete (Request $request){

		$id = $request->id;	
		$user = user::find($id);
		$user->delete();

		\Session::flash('flash_message', 'Data Berhasil Dihapus');
		
		return \Redirect::to('/admin/admin2/');
	}	
}
