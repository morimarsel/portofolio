<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\order_item;

class Order_itemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($order_id)
    {   

        $data = order_item::with('product')->where('order_id',$order_id)->get();

        // $data = order_item::all();

        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $request->validate([
            'product_id' => 'required|integer',
            'order_id' => 'required',
            'qty' => 'required|integer',
        ]);


        $data = $request->all();
        order_item::create($data);  
        


        
        // $order_item = new order_item;
        // $order_item->order_id = $request->input('order_id');
        // $order_item->product_id = $request->input('product_id');
        // $order_item->qty = $request->input('qty');
        // $order_item->save();        


        // $order_item = order_item::where('order_id', $order_item->order_id)->get();

        $order_item = order_item::with('product')->where('order_id', $data['order_id'])->get();

        return response()->json(['data' => $order_item]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $order_id)
    {
        
    $request->validate([
        'product_id' => 'required|integer',
        'order_id' => 'required',
        'qty' => 'required|integer',
    ]);  


    order_item::where('order_id', $order_id)
        ->where('id',$id)   
        ->update(['product_id' => $request->input('product_id'), 'order_id' => $request->input('order_id'), 'qty' => $request->input('qty') ])

    ;


    $data = order_item::with('product')->where('order_id', $request->input('order_id'))->where('id', $id)->get();

    return response()->json(['data' => $data]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $order_id)
    {
        order_item::where('order_id', $order_id)->where('id',$id)->delete();


        // $data = order_item::with('product')->where('order_id',$order_id)->get();

        // return response()->json(['data' => $data]);
        

        $data = order_item::with('product')->where('order_id',$order_id)->get();

        return response()->json(['data' => $data]);
    }
}
