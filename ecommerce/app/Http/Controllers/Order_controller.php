<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Models\product;
use App\Models\category;
use App\Models\order;
use App\Models\user;
use App\Models\order_item;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth; 
use Redirect;   
use Session;

class Order_controller extends Controller
{
    public function index ()
    {
        
        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;   

        $title ='Order';
        
        // return view('admin_view.admin1',['title' => $title]);
        
        $order = order::all();
        $user = user::all();

        // dd($categories->product);
        return view('admin_view.order_view', ['title' => $title, 'order' => $order, '
            user' => $user, 'username' => $username ] );             
    }

    public function insert (){
        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;
           
        $title ='Insert Order';
        
        $user = user::all();


        
        return view('admin_view.insert_order', ['title' => $title, 'user' => $user, 'username' => $username ] );
        

    }

    public function insertPost (Request $request){

        $username = $request->input('username');

        $validated = $request->validate([

            'user_id' => 'required',
            'tanggal_order' => 'required',
            

        ]);

        $order = new order;
        $order->user_id = $request->input('user_id');
        $order->tanggal_order = $request->input('tanggal_order');
        $order->save();

        $order_id = $order->id;
        $user = $request->user_id;
        $tanggal = $request->tanggal_order;

        return \Redirect::to('/admin/order/insert/item/'.$order_id.'/'.$tanggal.'/'.$user)->with(['username' => $username ]);
     
    }

    public function insert_item (Request $request)
    {   

        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

        $title ='Insert Item';

        $product = product::all();
        $user = $request->tanggal; //user id
        $tanggal = $request->user;
        $order = $request->id;

        $item = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->select('order_items.qty', 'products.nama')
            ->where('users.id', $user)
            ->where('orders.tanggal_order', $tanggal)
            ->where('order_items.order_id', $order)
            ->get()
        ;
        
        

        return view('admin_view.insert_item', ['title' => $title, 'product' => $product, 'item' => $item, 'user' => $user, 'tanggal' => $tanggal, 'order' => $order, 'username' => $username] );
    }

    

    public function insertAction (Request $request)
    {   

        $username = $request->input('username');
        $order_id = $request->input('order_id');
        $user = $request->input('user'); //user id
        $tanggal = $request->input('tanggal_order');



        $x = intval($request->get('product_id'));


        $request->merge([
            'product_id' => $x,
        ]);


          


        $validated = $request->validate([

            'product_id' => 'required|integer',
            'order_id' => 'required',
            'qty' => 'required|integer',
            

        ]);


        
        $order_item = new order_item;
        $order_item->order_id = $request->input('order_id');
        $order_item->product_id = $request->input('product_id');
        $order_item->qty = $request->input('qty');
        $order_item->save();        
        


        

        \Session::flash('flash_message', 'Item Berhasil Ditambah');

        return \Redirect::back()->with([
            'tanggal' => $tanggal,
            'user' => $user, 
            'order_id' => $order_id
        ]);
    }



    public function insert_edit(Request $request){

        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

        $title ='Edit';


        $product = product::all();
    

        $id = $request->id;

        $item = DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->join('users', 'users.id', '=', 'orders.user_id')
            ->select('order_items.qty', 'products.nama', 'order_items.id','order_items.order_id','orders.tanggal_order','users.id')
            ->where('order_items.id', $id)
            ->get()
        ;
        
        


        return view('admin_view.insert_edit', ['title' => $title, 'item' => $item, 'username' => $username,'id' => $id,'product' => $product] );

    }


    public function insert_editAction(Request $request){

        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

        $title ='Edit';


        
        $order_id = $request->input('order_id');
        $tanggal = $request->input('tanggal');
        $user = $request->input('user');


        $order_item = order_item::find($request->input('id'));
        $order_item->order_id = $order_id;
        $order_item->product_id = $request->input('product_id');
        $order_item->qty = $request->input('qty');
        $order_item->save();
        


        

        \Session::flash('flash_message', 'Item Berhasil Diedit');
        

        // return \Redirect::back()->with([
        //     'tanggal' => $tanggal,
        //     'user_id' => $user_id, 
        //     'order_id' => $order_id

        // ]);

        return \Redirect::to('/admin/order/insert/item/'.$order_id.'/'.$tanggal.'/'.$user)->with(['username' => $username ]);

    }




    public function edit (Request $request){

        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

        $title ='Edit Order';

        $order_id = $request->id;
        $tanggal = $request->tanggal_order;
        $name = $request->name;  


        
        $user_id = DB::table('users')->where('name', $name)->pluck('id');  


        $user = user::all();
        $product = product::all();

        $user_id = DB::table('users')->where('name', $name)->pluck('id');

        $data = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->select('orders.tanggal_order', 'order_items.qty', 'products.nama', 'order_items.order_id', 'orders.user_id', 'users.name')
            ->where('users.name', $name)
            ->where('orders.tanggal_order', $tanggal)
            ->get()
        ;



        
        return view('admin_view.edit_order', ['title' => $title, 'user' => $user, 'data' => $data, 'name' => $name, 'tanggal' => $tanggal, 'order_id' => $order_id, 'user_id' => $user_id, 'product' => $product, 'username' => $username ]);

    }

    

    // request itu syaratnya kalau pakai post
    public function editAction (Request $request)
    {   
        $username = $request->input('username');
        $tanggal = $request->input('tanggal');
        $order_id = $request->input('order_id'); 
        $name = $request->input('name');
        $user_id = $request->input('user_id');

        $validated = $request->validate([

            'tanggal' => 'required',
            'user_id' => 'required',

        ]);

        $id = $request->input('order_id'); 
        $order = order::find($id);

        

        $order->user_id = $request->input('user_id');
        $order->tanggal_order = $request->input('tanggal');
        $order->save();


        $id = $request->input('order_id'); 
        $order = order::find($id);

        
        \Session::flash('message_update_user', 'User Berhasil Diubah');

        // return \Redirect::back()->with([
        //     'tanggal' => $tanggal, 
        //     'order_id' => $order_id
        // ]);

      
        return \Redirect::to('/admin/order');
    }










    public function edit_item (Request $request){

        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

        $title ='Edit Item';

        $username = $request->input('username');
        $name = $request->name;


        if (is_null($request->id) == 0 AND  is_null($request->tanggal_order) == 0 AND is_null($request->user_id) == 0 )
        {
            $order_id = $request->id;
            $tanggal = $request->tanggal_order;
            $user_id = $request->user_id;    
        }elseif ( is_null( Session::get('user_id') ) == 0 AND is_null( Session::get('tanggal') ) == 0 AND is_null( Session::get('order_id') ) == 0  ) 
        {
            
            $user_id = Session::get('user_id');
            $tanggal = Session::get('tanggal');
            $order_id = Session::get('order_id');

        }

        


        $product = product::all();


        $data = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->select('orders.tanggal_order', 'order_items.qty', 'products.nama', 'order_items.order_id', 'order_items.id','orders.user_id', 'users.name')
            ->where('users.name', $name)
            ->where('orders.tanggal_order', $tanggal)
            ->get()
        ;



        
        return view('admin_view.edit_item', ['title' => $title, 'name' => $name, 'tanggal' => $tanggal, 'order_id' => $order_id, 'product' => $product, 'data' => $data, 'user_id' => $user_id, 'username' => $username  ]);

    }


    public function editAction_item (Request $request)
    {   

        $username = $request->input('username');
        $order_id = $request->input('order_id');
        $user_id = $request->input('user_id');
        $tanggal = $request->input('tanggal');
        



        $x = intval($request->get('product_id'));


        $request->merge([
            'product_id' => $x,
        ]);


          


        $validated = $request->validate([

            'product_id' => 'required|integer',
            'order_id' => 'required',
            'qty' => 'required|integer',
            

        ]);


        
        $order_item = new order_item;
        $order_item->order_id = $request->input('order_id');
        $order_item->product_id = $request->input('product_id');
        $order_item->qty = $request->input('qty');
        $order_item->save();        
        


        

        \Session::flash('message_update_item', 'Item Berhasil Ditambah');

        return \Redirect::back()->with([
            'tanggal' => $tanggal,
            'user_id' => $user_id, 
            'order_id' => $order_id

        ]);


        
    }




    public function delete (Request $request){

        

        $order_id = $request->id;
        $tanggal = $request->tanggal_order;
        $name = $request->name;  


        
        $user_id = DB::table('users')->where('name', $name)->pluck('id');

        $data = DB::table('orders')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->select('order_items.id')
            ->where('users.name', $name)
            ->where('orders.tanggal_order', $tanggal)
            ->get()
        ;


        foreach ($data as $row) {
           
            $id = $row->id;
            $order_item = order_item::find($id);
            $order_item->delete();
        }

    


        $order = order::find($order_id);
        $order->delete();






        \Session::flash('flash_message', 'Data Berhasil Dihapus');

        return \Redirect::back();
    }



    public function delete_item (Request $request){


        $id = $request->item_id;
        $order_item = order_item::find($id);
        $order_item->delete();


        \Session::flash('message_update_item', 'Item Berhasil Dihapus');

        return \Redirect::back();
    }



}

