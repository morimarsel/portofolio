<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class Checkout_controller extends Controller
{
    public function index ()
	{
		
		$title = 'checkout';

		if (isset(Auth::user()->name)) {

			isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

			return view('divisima.checkout', ['title' => $title, 'username' => $username ] ); 
			
		}else{
			return view('divisima.checkout', ['title' => $title] ); 
		} 

	}
}
