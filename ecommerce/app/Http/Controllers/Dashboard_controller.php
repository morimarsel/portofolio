<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Dashboard_controller extends Controller
{
    public function index ()
    {   

        isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;


        return view('admin_view.dashboard', compact('username'));

    }
}
