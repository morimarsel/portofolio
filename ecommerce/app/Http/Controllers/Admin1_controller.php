<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Models\product;
use App\Models\category;
use App\Models\user;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator; 
use Illuminate\Support\Facades\Auth;
use Redirect;	

class Admin1_controller extends Controller
{	
    public function index ()
	{
		isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;	

		$title ='AdminLTE 3 | Dashboard 1';
		
		// return view('admin_view.admin1',['title' => $title]);
		
		$product = product::all();
		$categories = category::all();

		// dd($categories->product);
		return view('admin_view.admin1', ['title' => $title, 'product' => $product, '
			categories' => $categories, 'username' => $username ] );				
	}

	public function insert (){

		$categories = category::get();
		
		return view('admin_view.insert_admin1',compact('categories'));

	}

	// request itu syaratnya kalau pakai post
	public function insertAction (Request $request)
	{	

		// nama namnya harus sama dengan name di form post
		// $request itu kayak variabe globalnya
		$validated = $request->validate([

			'code' => 'required|unique:products',
			'nama' => 'required',
			'stock' => 'required|integer',
			'varian' => 'required',
			'category_id' => 'required',
			'keterangan' => 'required',

		]);

		// kalau isian salah keputus disini


		$product = new product;
		$product->code = $request->input('code');
		$product->nama = $request->input('nama');
		$product->stock = $request->input('stock');
		$product->varian = $request->input('varian');
		$product->category_id = $request->input('category_id');
		$product->keterangan = $request->input('keterangan');
		$product->save();
		
		

		return \Redirect::to('/admin/admin1/')->with('alert', 'Sukses');
	}


	public function edit (Request $request){

		$id = $request->id;	
		$product = product::find($id);
		$categories = category::get();

		
		
		return view('admin_view.edit_admin1',compact('product','categories'));

	}

	

	// request itu syaratnya kalau pakai post
	public function editAction (Request $request)
	{	
		$validated = $request->validate([

			'nama' => 'required',
			'stock' => 'required|integer',
			'varian' => 'required',
			'category_id' => 'required',
			'keterangan' => 'required',

		]);

		$id = $request->id;	
		$product = product::find($id);

		if ( $product->code == $request->input('code')) {
			
			$product->code = "";
			$product->save();

		}

		

		// nama namnya harus sama dengan name di form post
		// $request itu kayak variabe globalnya
		$validated = $request->validate([

			'code' => 'required|unique:products,code',

		]);

		// kalau isian salah keputus disini

		

		$product->code = $request->input('code');
		$product->nama = $request->input('nama');
		$product->stock = $request->input('stock');
		$product->varian = $request->input('varian');
		$product->category_id = $request->input('category_id');
		$product->keterangan = $request->input('keterangan');
		$product->save();
		
		

		return \Redirect::to('/admin/admin1/')->with('alert_update', 'Sukses');
	}


	public function delete (Request $request){

		$id = $request->id;	
		$product = product::find($id);
		$product->delete();

		\Session::flash('flash_message', 'Data Berhasil Dihapus');
		
		return \Redirect::to('/admin/admin1/');

	}
}	





