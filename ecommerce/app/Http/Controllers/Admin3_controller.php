<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin3_controller extends Controller
{
    public function index ()
	{


		isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;


		$title='AdminLTE 3 | Dashboard 3';
		

		return view('admin_view.admin3', ['title' => $title, 'username' => $username ]);
    }
}
