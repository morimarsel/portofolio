<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Home_controller extends Controller
{

	public function index ()
	{
		
		$title = 'home';
		

		if (isset(Auth::user()->name)) {

			isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;

			return view('divisima.index', ['title' => $title, 'username' => $username ] ); 
			
		}else{
			return view('divisima.index', ['title' => $title] ); 
		} 
	}

	
}
