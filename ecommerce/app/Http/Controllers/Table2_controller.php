<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Models\product;
use App\Models\category;
use App\Models\user;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Redirect;		

class Table2_controller extends Controller
{
    public function index ()
	{	

		isset(Auth::user()->name) ? $username = Auth::user()->name : $username = Auth::user()->email;
		
		$title ='AdminLTE 3 | Table 2';
		
		$user = user::all();
		

		return view('admin_view.table2', ['title' => $title, 'user' => $user, 'username' => $username ] );

	}
}
