<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
class orders_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 150; $i++) {
          $userData[] = [
              'tanggal_order' => Carbon::parse(rand(1,2021).'-'.rand(1,12).'-'.rand(1,31)),  
              'user_id' => rand(1,100)
          ];
        }

        foreach ($userData as $user) {
            DB::table('orders')->insert($user);
        }
    }
}
