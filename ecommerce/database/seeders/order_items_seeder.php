<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class order_items_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 200; $i++) {
          $userData[] = [

                'order_id' => rand(1,150),
                'product_id' => rand(1,100),
                'qty' => rand(1,10)
          ];
        }

        foreach ($userData as $user) {
            DB::table('order_items')->insert($user);
        }
    }
}
