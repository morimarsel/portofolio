<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class products_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {



  //   	DB::table('products')->insert([	

  //   		for ($i=0; $i < 80; $i++) {

		// 		'code' => rand(10,100),
	 //            'nama' => Str::random(10),
	 //            'stock' => rand(1,10),
	 //            'varian' => Str::random(10),
	 //            'keterangan' => Str::random(10)

  //           }
		// ]);

  	  for ($i=0; $i < 100; $i++) {
          $userData[] = [
              'code' => rand(10,9999),
              'nama' => Str::random(10),
              'stock' => rand(1,10),
              'varian' => Str::random(10),
              'keterangan' => Str::random(10),
              'category_id' => rand(1,5)
          ];
      }

      foreach ($userData as $user) {
          DB::table('products')->insert($user);
      }
    }
}

