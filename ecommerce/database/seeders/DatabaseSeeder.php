<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([

            
            categories_seeder::class,
        	products_seeder::class,
            users_seeder::class,
            orders_seeder::class,
            order_items_seeder::class,
            

        ]);
    }
}
