<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class users_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
     	for ($i=0; $i < 100; $i++) {
            $userData[] = [
                'name' => Str::random(10),
                'email' => Str::random(10,32)."@Yahoo.com",
                'password' => Hash::make('password'),
            ];

        }

        foreach ($userData as $user) {
            DB::table('users')->insert($user);
        }
    }
}
