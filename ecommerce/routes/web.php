<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home_controller;
use App\Http\Controllers\Checkout_controller;
use App\Http\Controllers\Cart_controller;
use App\Http\Controllers\Contact_controller;
use App\Http\Controllers\Product_controller;
use App\Http\Controllers\Category_controller;
use App\Http\Controllers\Admin1_controller;
use App\Http\Controllers\Admin2_controller;
use App\Http\Controllers\Admin3_controller;
use App\Http\Controllers\Order_controller;
use App\Http\Controllers\Table1_controller;
use App\Http\Controllers\Table2_controller;
use App\Http\Controllers\Table3_controller;
use App\Http\Controllers\Dashboard_controller;
use App\Http\Controllers\Auth\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/',[App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('/',[App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Route::get('/register',[App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register',[App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register');


Route::middleware(['auth', 'is_admin'])->group(function(){
	Route::prefix('/admin')->group(function () 	
	{

		// Route::view('/', 'admin_view.dashboard');	
		Route::get('/',[Dashboard_controller::class, 'index']);



		Route::prefix('/admin1')->group(function () 	
		{
			Route::get('/', [Admin1_controller::class, 'index']);	

			//insert proses
			Route::get('/insert',[Admin1_controller::class, 'insert']);
			Route::post('/insert',[Admin1_controller::class, 'insertAction']);
			Route::get('/edit/{id}',[Admin1_controller::class, 'edit']);
			Route::post('/edit/{id}',[Admin1_controller::class, 'editAction']);
			Route::get('/delete/{id}',[Admin1_controller::class, 'delete']);

		});


		Route::prefix('/admin2')->group(function () 	
		{

			Route::get('/', [Admin2_controller::class, 'index']);	
			
			//insert proses
			Route::get('/insert',[Admin2_controller::class, 'insert']);
			Route::post('/insert',[Admin2_controller::class, 'insertAction']);		
			Route::get('/edit/{id}',[Admin2_controller::class, 'edit']);
			Route::post('/edit/{id}',[Admin2_controller::class, 'editAction']);
			Route::get('/delete/{id}',[Admin2_controller::class, 'delete']);
		});







	 
		Route::prefix('/order')->group(function () 	
		{
			Route::get('/', [Order_controller::class, 'index']);	

			//insert proses
			Route::prefix('/insert')->group(function () 	
			{
				Route::get('/',[Order_controller::class, 'insert']);
				Route::post('/',[Order_controller::class, 'insertPost']);
				
				Route::get('/item/{id}/{tanggal}/{user}',[Order_controller::class, 'insert_item']);
				Route::post('/item',[Order_controller::class, 'insertAction']);
				Route::get('/item/edit/{id}',[Order_controller::class, 'insert_edit']);
				Route::post('/item/edit',[Order_controller::class, 'insert_editAction']);
			});

			Route::prefix('/edit')->group(function () 	
			{
				Route::get('/{id}/{name}/{tanggal_order}',[Order_controller::class, 'edit']);
				Route::post('/',[Order_controller::class, 'editAction']);




				Route::get('/edit_item/{id}/{name}/{tanggal_order}/{user_id}',[Order_controller::class, 'edit_item']);
				Route::post('/edit_item',[Order_controller::class, 'editAction_item']);


				Route::get('/edit_item/delete/{id}/{name}/{tanggal_order}/{user_id}/{item_id}',[Order_controller::class, 'delete_item']);

			});

			Route::get('/delete/{id}/{name}/{tanggal_order}',[Order_controller::class, 'delete']);
		});












		//Admin View	
		Route::get('/admin3', [Admin3_controller::class, 'index']);	

		//Table View
		Route::get('/table1', [Table1_controller::class, 'index']);	
		Route::get('/table2', [Table2_controller::class, 'index']);	
		Route::get('/table3', [Table3_controller::class, 'index']);	

	});
});



//Pages View
	Route::get('/home', [Home_controller::class, 'index']);
	Route::get('/product', [Product_controller::class, 'index']);
	Route::get('/checkout', [Checkout_controller::class, 'index']);
	Route::get('/cart', [Cart_controller::class, 'index']);
	Route::get('/contact', [Contact_controller::class, 'index']);
	Route::get('/category', [Category_controller::class, 'index']);	







//Jangan dihapus mas buat Doc ku

// Route::view('/table1', 'admin_view.table1');
// Route::view('/table2', 'admin_view.table2');
// Route::view('/table3', 'admin_view.table3');

// Route::view('/admin2', 'admin_view.admin2');
// Route::view('/admin3', 'admin_view.admin3');


Route::get('/status', [App\Http\Controllers\HomeController::class, 'index']);
// ->name('home');
