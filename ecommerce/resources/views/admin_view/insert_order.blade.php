@extends('layout.admin_template')

@section('title')

{{$title}}

@endsection


@section('content')

  @php

  
  @endphp

  



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif 
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
             <!--  <div class="card-header">
                
                <a href="/admin/order/insert" class="btn btn-success">INSERT</a>

              </div> -->
              <!-- /.card-header -->
              <div class="card-body">

                <div class="row">
                  <div class="col-6"><label style="font-weight: bold;">Nama:</label></div>
                  <div class="col-6"><label style="font-weight: bold;">Tanggal Order:</label></div>
                </div>

                <form method="post" action="/admin/order/insert">
  
                  @csrf   

                  <input type="hidden" name="order_id" value="{{$username}}">
                  <div class="row">

                    <div class="col-6">
                        <select name="user_id" style="height: 50PX; width: 350PX;">
                          
                          @foreach( $user as $row )
                          <option value="{{ $row->id }}"><?php echo $row->name; ?></option>
                          @endforeach
                          
                        </select>
                    </div>

                    <div class="col-6">
                      <input type="date" name="tanggal_order" style="width: 350px; height: 50px;">
                    </div>

                  </div>
                      <br><br>                  
                      <input class="btn btn-success" type="submit" value="Buat Orderan" required>
                </form>

                


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection