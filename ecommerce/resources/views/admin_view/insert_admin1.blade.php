@extends('layout.insert_admin_template')

@section('title')

Menambah Data Product

@endsection

<!-- untuk menampilkan error -->
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
	

@section('content')


<!-- general form elements -->
<div class="card card-primary">
	<div class="card-header">
	    <h3 class="card-title">Data Product</h3>
		</div>
	<!-- /.card-header -->
	<!-- form start -->



	<!-- formnya -->
	<form method="post" action="/admin/admin1/insert">

		<!-- untuk keamanan -->
		@csrf

	    <div class="card-body">

	    
	    	<div class="form-group">
		        <label>Kategori</label>
		        <select name="category_id" class="form-control">
					@foreach ( $categories as $row)
						<option value="{{$row->id}}"> {{$row->nama}} </option>
					@endforeach
				</select>
		    </div>
		     <div class="form-group">
		        <label>Nama</label>
		        <input type="text" name="nama" class="form-control" value="{{old('nama')}}" placeholder="Masukkan Nama">
		    </div>
		    <div class="form-group">
		        <label>Code</label>
		        <input type="text" name="code" class="form-control" value="{{old('code')}}" placeholder="Masukkan Code">
		    </div>
		    <div class="form-group">
		        <label>Stock</label>
		        <input type="number" class="form-control" name="stock" value="{{old('stock')}}" placeholder="Masukkan Jumlah Stock">
		    </div>
		    <div class="form-group">
		        <label>Varian</label>
		        <input type="text" class="form-control" name="varian" value="{{old('varian')}}" placeholder="Masukkan Jenis Varian">
		    </div>
		    <div class="form-group">
		        <label>keterangan</label>
		        <input type="text" class="form-control" name="keterangan" value="{{old('keterangan')}}" placeholder="Masukkan keterangan Barang">
		    </div>
	    </div>
	    <!-- /.card-body -->

	    <div class="card-footer">
	      <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
	</form>
	<a href="/admin/admin1" class="btn btn-danger">Kembali</a> 
</div>
@endsection
