@extends('layout.admin_template')

@section('title')

Selamat Datang

@endsection


@section('content')

  @php

  
  @endphp




  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header mx-auto">
                <span class="font-weight-bold" >SELAMAT DATANG DI MENU ADMIN</span>
              </div>
              <!-- /.card-header -->
              <div class="mx-auto card-body">
                
                <img  src="{{asset('lisa2.jpg')}}">
                <br><br>

                <form id="formLogout" method="post" action="{{ route('logout') }}">
                  @csrf
                  <input class="btn btn-danger" type="submit" value="Logout" onclick="return confirm('Yakin ingin Logout?');" style="width: 100%;">

                </form>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection


