@extends('layout.admin_template')

@section('title')

{{$title}}

@endsection



@section('content')

  @php

  
  @endphp



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif        
          @if (Session::has('message_update_user'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong style="color: red; z-index: 1">{{ Session::get('message_update_user') }}</strong>
            </div>
          @endif
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Item</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-6"><label style="font-weight: bold;">Pemesan</label></div>
                  <div class="col-6"><label style="font-weight: bold;">Tanggal Pesan</label></div>
                </div>

                <form method="post" action="/admin/order/edit">

                  @csrf
                  <input type="hidden" name="order_id" value="{{$username}}">
                  <input type="hidden" name="order_id" value="{{old('order_id',$order_id)}}">
                  <input type="hidden" name="name" value="{{old('name',$name)}}">
                  <div class="row">

                    <div class="col-6">
                      <select name="user_id" style="height: 50PX; width: 350PX;">
                          
                        @foreach( $user as $row )
                        <option value="{{ $row->id }}"

                          <?php if( $row->name == $name )
                            {
                              echo "selected";
                            }
                          ?>

                          ><?php echo $row->name; ?></option>
                        @endforeach
              
                      </select>
                    </div>

                    <div class="col-6">

                      <input type="date" name="tanggal" style="width: 350px; height: 50px;" value="{{ old('tanggal',$tanggal) }}">

                    </div>
                  </div>
                  <br><br>
                  <div class="row">
                    <div class="col-12">

                      <input class="btn btn-success mb-2" type="submit" value="Ganti Data User" onclick="return confirm('Yakin ingin mengganti?');" style="width: 100%;">

                    </div>                 
                  </div>                   
                  <br>

                </form>


                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Product</th>
                    <th>QTY</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php ($i = 1)
                  @foreach( $data as $row )
                  <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $row->nama }} </td>
                    <td> {{ $row->qty }} </td>
                  </tr>
                  @endforeach

                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Product</th>
                      <th>QTY</th>
                    </tr>
                  </tfoot>
                </table>
                <a href="/admin/order/edit/edit_item/{{$order_id}}/{{$name}}/{{$tanggal}}/{{$user_id}}" class="btn btn-primary"> Edit Item</a>
              </div>
              <!-- /.card-body -->
              <a href="/admin/order" class="btn btn-primary"> Kembali</a>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection