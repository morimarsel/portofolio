@extends('layout.insert_admin_template')

@section('title')

Menambah Data User

@endsection

<!-- untuk menampilkan error -->
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
	

@section('content')

<!-- general form elements -->
<div class="card card-primary">
	<div class="card-header">
	    <h3 class="card-title">Data User</h3>
		</div>
	<!-- /.card-header -->
	<!-- form start -->



	<!-- formnya -->
	<form method="post" action="/admin/admin2/insert">

		<!-- untuk keamanan -->
		@csrf

	    <div class="card-body">

	    

		    <div class="form-group">
		        <label>Nama</label>
		        <input type="text" name="nama" class="form-control" value="{{old('nama')}}" placeholder="Masukkan Nama">
		    </div>
		    <div class="form-group">
		        <label>E-mail</label>
		        <input type="text" name="email" class="form-control" value="{{old('email')}}" placeholder="Masukkan E-mail">
		    </div>
		    <div class="form-group">
		        <label>Password</label>
		        <input type="password" name="password" class="form-control" value="{{old('password')}}" placeholder="Masukkan Password" id="myInput">
		        <input type="checkbox" onclick="myFunction()">Show Password
		    </div>
		    
	    </div>
	    <!-- /.card-body -->

	    <div class="card-footer">
	      <button type="submit" class="btn btn-primary">Submit</button>
	    </div>
	</form>
	<a href="/admin/admin1" class="btn btn-danger">Kembali</a> 
</div>

@endsection

