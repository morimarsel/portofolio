  @extends('layout.admin_template')

@section('title')

{{$title}}

@endsection



@section('content')

  @php

  
  @endphp



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif        
          @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong style="color: red; z-index: 1">{{ Session::get('flash_message') }}</strong>
            </div>
          @endif
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                      <span style="font-weight: bold; font-size: 30px;">Informasi data yang ingin diubah</span>
                      <br>
                      @foreach( $item as $row )
                      <p>Nama Product: {{$row->nama}}</p>
                      <p>Qty: {{$row->qty}} </p>
                      @endforeach
                  </div>  
                </div>

                <span style="font-weight: bold; font-size: 30px;">Form Perubahan</span>
                <div class="row">
                  
                  <br>
                  <div class="col-6"><label for="product_id" style="font-weight: bold;">Product:</label></div>
                  <div class="col-6"><label for="qty" style="font-weight: bold;">Qty</label></div>
                </div>


                
                
                <form method="post" action="/admin/order/insert/item/edit" id="form-insert-item">

                  @csrf

                  <input type="hidden" name="id" value="{{$id}}">
                  @foreach( $item as $row )
                  <input type="hidden" name="order_id" value="{{$row->order_id}}">
                  <input type="hidden" name="tanggal" value="{{$row->tanggal_order}}">
                  <input type="hidden" name="user" value="{{$row->id}}">
                  @endforeach
                  <div class="row">

                    <div class="col-6">
                        <select id="product_id" name="product_id" style="height: 50PX; width: 350PX;">
                          @foreach( $product as $row )
                          <option value="{{ $row->id }}">{{ $row->nama }}</option>
                          @endforeach
                          
                    
                        </select>
                    </div>

                    <div class="col-6">
                      <input id="qty" type="number" name="qty" style="width: 350px; height: 50px;">
                    </div>

                  </div>
                      <br><br>                  
                      <input class="btn btn-success" type="submit" value="Submit" name="submit" id="submit">
                </form>
                
              </div>
              <!-- /.card-body -->
              <a href="../" class="btn btn-danger" onclick="return confirm('Yakin Sudah Selesai?');"> Kembali</a> </td>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection
