  @extends('layout.admin_template')

@section('title')

{{$title}}

@endsection



@section('content')

  @php

  
  @endphp



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif        
          @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong style="color: red; z-index: 1">{{ Session::get('flash_message') }}</strong>
            </div>
          @endif
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-6"><label for="product_id" style="font-weight: bold;">Product:</label></div>
                  <div class="col-6"><label for="qty" style="font-weight: bold;">Qty</label></div>
                </div>

                
                <form method="post" action="/admin/order/insert/item" id="form-insert-item">

                  @csrf

                  <input type="hidden" name="username" value="{{$username}}">
                  <input type="hidden" name="order_id" value="{{$order}}" id="order_id">
                  <input type="hidden" name="user" value="{{$user}}">
                  <input type="hidden" name="tanggal_order" value="{{$tanggal}}">
                  <div class="row">

                    <div class="col-6">
                        <select id="product_id" name="product_id" style="height: 50PX; width: 350PX;">
                          @foreach( $product as $row )
                          <option value="{{ $row->id }}">{{ $row->nama }}</option>
                          @endforeach
                          
                    
                        </select>
                    </div>

                    <div class="col-6">
                      <input id="qty" type="number" name="qty" style="width: 350px; height: 50px;">
                    </div>

                  </div>
                      <br><br>                  
                      <input class="btn btn-success" type="submit" value="Submit" name="submit" id="submit">
                </form>
                


                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>QTY</th>
                    <th>Aksi</th>
                    
                  </tr>
                  </thead>
                  <tbody id="data-item">

                  @php ($i = 1)
                  @foreach( $item as $row )
                  <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $row->nama }} </td>
                    <td> {{ $row->qty }} </td>
                    

                    <td> <a href="/admin/order/insert/item/edit/{{$row->id}}" class="btn btn-success"> Edit</a> 
                    |
                    <a href="" class="btn btn-danger"> Delete</a> </td>
                  </tr>
                  @endforeach

                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>QTY</th>
                    <th>Aksi</th>
                    
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
              <a href="/admin/order" class="btn btn-danger" onclick="return confirm('Yakin Sudah Selesai?');"> Selesai</a> </td>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>


<script type="text/javascript">
    $(document).ready(function(){
      $("#form-insert-item").on('submit',function(e){
        e.preventDefault();
        
        const order_id = $('#order_id').val();
        const product_id = $('#product_id').val();
        const qty = $('#qty').val();

        $.ajax({
          type: 'POST',
          url: "/api/order/insert/item/",
          data: {
            'order_id': order_id,
            'product_id': product_id,
            'qty':qty
          },

          success: function(result){
            $('#data-item').html(updatetable(result.data));
            $('#qty').val('');
            // console.log(updatetable(result.data));
            // updatetable(result.data);
          }

        })
      })

    })

    $(document).on('click', function(e){
      

      if($(e.target).hasClass('btn-delete')){

        confirm('Yakin ingin menghapus?');  
        const order_id = $(e.target).data('order-id');
        const id = $(e.target).data('id');
        $.ajax({
          type: 'DELETE',
          url: `/api/order/delete/${id}/${order_id}`,
          success: function(result){

            $('#data-item').html(updatetable(result.data));
            
          }

        })
      }

    });



    function updatetable(data){
      let table = '';
      data.forEach((d,i) => { 
        table += `
          <tr>
            <td> ${i+1} </td>
            <td> ${d.product.nama} </td>
            <td> ${d.qty} </td>
            <td> 
              <a href="/admin/order/insert/item/edit/${d.id}" class="btn btn-success"> Edit</a> 

              |

              <a type="button" class="btn btn-danger btn-delete"
              data-order-id="${d.order_id}"
              data-id="${d.id}"> Delete</a>
            </td>
          </tr>
        `;   
      })

    return table;
  }
</script>
@endsection
