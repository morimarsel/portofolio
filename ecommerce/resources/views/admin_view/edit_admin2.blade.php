@extends('layout.insert_admin_template')

@section('title')

Mengubah Data User

@endsection

<!-- untuk menampilkan error -->
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
	
@section('content')


<!-- general form elements -->
<div class="card card-primary">
	<div class="card-header">
	    <h3 class="card-title">Edit Data user</h3>
		</div>
	<!-- /.card-header -->
	<!-- form start -->



	<!-- formnya -->
	<form method="post" action="/admin/admin2/edit/{{$user->id}}">

		<!-- untuk keamanan -->
		@csrf

	    <div class="card-body">

	    
		    
		    <div class="form-group">
		        <label>Nama</label>
		        <input type="text" name="nama" class="form-control" value="{{old('nama',$user->name)}}" placeholder="Masukkan nama">
		    </div>
		    <div class="form-group">
		        <label>email</label>
		        <input type="text" name="email" class="form-control" value="{{old('email',$user->email)}}" placeholder="Masukkan email">
		    </div>
		    <div class="form-group">
		        <label>password</label>
		        <input type="text" class="form-control" name="password" value="{{$user->password}}" placeholder="Masukkan Jenis password">
		    </div>
	    </div>
	    <!-- /.card-body -->

	    <div class="card-footer">
	      <button type="submit" class="btn btn-primary">Edit</button>
	    </div>
	</form>
	<a href="/admin/admin2" class="btn btn-danger">Kembali</a> 
</div>
@endsection
