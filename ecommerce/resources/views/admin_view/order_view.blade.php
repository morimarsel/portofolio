@extends('layout.admin_template')

@section('title')

{{$title}}

@endsection


@section('content')

  @php

  
  @endphp




   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
          @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong style="color: red; z-index: 1">{{ Session::get('flash_message') }}</strong>
            </div>
          @endif
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order Table</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                
                <a href="/admin/order/insert" class="btn btn-success">INSERT</a>

              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Pemesan</th>
                    <th>Tanggal Order</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php ($i = 1)
                  @foreach( $order as $row )
                  <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $row->user->name }} </td>
                    <td> {{ $row->tanggal_order }} </td>
                    <td> <a href="/admin/order/edit/{{$row->id}}/{{$row->user->name}}/{{$row->tanggal_order}}" class="btn btn-success"> Edit</a> 
                    |
                    <a href="/admin/order/delete/{{$row->id}}/{{$row->user->name}}/{{$row->tanggal_order}}" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus?');"> Delete</a> </td>
                  </tr>
                  @endforeach

                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Tanggal Order</th>
                    <th>Aksi</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection